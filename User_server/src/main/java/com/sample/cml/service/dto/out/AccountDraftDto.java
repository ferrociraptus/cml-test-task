package com.sample.cml.service.dto.out;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.UUID;

public class AccountDraftDto {

    private UUID userId;
    private String login;
    private String password;

    @JsonCreator
    public AccountDraftDto(UUID userId, String login, String password) {
        this.userId = userId;
        this.login = login;
        this.password = password;
    }

    protected AccountDraftDto() {}

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
