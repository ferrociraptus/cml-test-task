package com.sample.cml.service.dto.out;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.UUID;

public class UserDto {

    private UUID id;
    private String name;
    private String post;

    @JsonCreator
    public UserDto(UUID id, String name, String post) {
        this.id = id;
        this.name = name;
        this.post = post;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
