package com.sample.cml.service.security;

import com.sample.cml.service.dto.out.TokenDto;
import com.sample.cml.service.dto.out.TokenStatus;
import reactor.core.publisher.Mono;

public interface AuthService {

    Mono<TokenDto> authenticateUser(String login, String password);

    Mono<TokenDto> getSessionJwt(String refreshToken);

    Mono<Void> logout(String refreshToken);

    Mono<Void> logout(String refreshToken, String sessionToken);

    Mono<TokenStatus> validateToken(String anyToken);
}
