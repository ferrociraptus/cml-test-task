package com.sample.cml.service.security.impl;

import com.sample.cml.repository.UserRepository;
import com.sample.cml.service.config.JwtConfig;
import com.sample.cml.service.security.AuthService;
import com.sample.cml.service.security.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.util.Logger;
import reactor.util.Loggers;

@Service
public class AuthServiceImpl {
    private final UserRepository accountRepository;

    private final JwtService jwtService;

    private static final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    private final Logger logger = Loggers.getLogger(AuthService.class);


    @Autowired
    public AuthServiceImpl(
            UserRepository accountRepository,
            JwtService jwtService,
            JwtConfig jwtConfig
    ) {
        this.accountRepository = accountRepository;
        this.jwtService = jwtService;
    }
}
