package com.sample.cml.service.dto;

public enum TokenType {
    SessionToken,
    IdToken
}
