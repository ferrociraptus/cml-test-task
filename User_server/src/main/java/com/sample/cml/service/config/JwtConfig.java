package com.sample.cml.service.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class JwtConfig {

    @Value("${cml.jwt.rsa-public-key-file}")
    private String publicKeyFilePath;
    public JwtConfig() {
    }

    public String getPublicKeyFilePath() {
        return publicKeyFilePath;
    }
}
