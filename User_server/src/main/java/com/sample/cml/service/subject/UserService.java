package com.sample.cml.service.subject;

import com.sample.cml.service.dto.in.UserDraftDto;
import com.sample.cml.service.entity.User;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

public interface UserService {

    Mono<User> createUser(UserDraftDto accountDraft);

    Mono<Boolean> deleteUserById(UUID id);

    Mono<List<User>> getAll();

    Mono<User> getById(UUID id);
}
