package com.sample.cml.service.subject.impl;

import com.sample.cml.client.AccountServiceClient;
import com.sample.cml.repository.UserRepository;
import com.sample.cml.service.dto.in.UserDraftDto;
import com.sample.cml.service.dto.out.AccountDraftDto;
import com.sample.cml.service.entity.User;
import com.sample.cml.service.subject.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.util.Logger;
import reactor.util.Loggers;

import java.util.List;
import java.util.UUID;


@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final AccountServiceClient accountServiceClient;
    private final Logger logger = Loggers.getLogger(UserService.class);

    @Autowired
    public UserServiceImpl(
            UserRepository repository, AccountServiceClient accountServiceClient) {
        this.repository = repository;
        this.accountServiceClient = accountServiceClient;
    }

    @Override
    public Mono<User> createUser(UserDraftDto accountDraft) {
        return repository.create(
                new User(accountDraft.getName(), accountDraft.getPassword())
        ).doOnEach(userSignal ->
            accountServiceClient.createAccount(
                    new AccountDraftDto(
                            userSignal.get().getId(),
                            accountDraft.getLogin(),
                            accountDraft.getPassword()
                    )
            )
        ).onErrorComplete().doOnEach(u -> repository.deleteById(u.get().getId()));
    }

    @Override
    public Mono<Boolean> deleteUserById(UUID id) {
        // TODO: add verivied remove the account
        // Account first then User because the user can store sensitive data in web of data tables
        return repository.deleteById(id);
    }

    @Override
    public Mono<List<User>> getAll() {
        return repository.getAll();
    }

    @Override
    public Mono<User> getById(UUID id) {
        return repository.getById(id);
    }
}
