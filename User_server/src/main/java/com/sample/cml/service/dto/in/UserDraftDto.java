package com.sample.cml.service.dto.in;

import com.fasterxml.jackson.annotation.JsonCreator;

public class UserDraftDto {

    private String name;
    private String post;
    private String login;
    private String password;

    @JsonCreator
    public UserDraftDto(String name, String post, String login, String password) {
        this.name = name;
        this.post = post;
        this.password = password;
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
