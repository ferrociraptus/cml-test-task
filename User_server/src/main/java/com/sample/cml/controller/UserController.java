package com.sample.cml.controller;

import com.sample.cml.service.dto.in.UserDraftDto;
import com.sample.cml.service.dto.out.UserDto;
import com.sample.cml.service.entity.User;
import com.sample.cml.service.subject.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/v1")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/user")
    public Mono<UserDto> createUser(UserDraftDto userDraft) {
        return userService.createUser(userDraft)
                .map(UserController::userToDto);
    }

    @PostMapping("/user/{id}")
    public Mono<UserDto> getUserById(@RequestParam UUID id) {
        return userService.getById(id)
                .map(UserController::userToDto);
    }

    @GetMapping("/users")
    public Mono<List<UserDto>> getUsers() {
        return userService.getAll()
                .map(l -> l.stream().map(UserController::userToDto).toList());
    }

    public static UserDto userToDto(User u) {
        return new UserDto(u.getId(), u.getFullName(), u.getPost());
    }
}
