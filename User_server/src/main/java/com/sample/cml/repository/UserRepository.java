package com.sample.cml.repository;

import com.sample.cml.service.entity.User;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

abstract public class UserRepository implements AsyncCrudInterface<UUID, User> {

    public abstract Mono<List<User>> getAll();
}
