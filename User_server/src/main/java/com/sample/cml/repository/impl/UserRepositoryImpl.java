package com.sample.cml.repository.impl;

import com.sample.cml.repository.UserRepository;
import com.sample.cml.service.entity.User;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.converters.uni.UniReactorConverters;
import jakarta.persistence.criteria.CriteriaDelete;
import jakarta.persistence.criteria.Root;
import org.hibernate.reactive.mutiny.Mutiny;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;
import java.util.UUID;


@Repository
public class UserRepositoryImpl extends UserRepository {

    private final Mutiny.SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(Mutiny.SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Mono<User> getById(UUID uuid) {
        return uniGetById(uuid)
                .convert().with(UniReactorConverters.toMono());
    }

    @Override
    public Mono<User> create(User obj) {
        return sessionFactory.withSession(session ->
                    session.persist(obj).onItem()
                            .call(v -> session.flush())
            ).onItem().transform(v -> obj)
            .convert().with(UniReactorConverters.toMono());
    }

    @Override
    public Mono<User> update(User obj) {
        return uniGetById(obj.getId()).onItem().transformToUni( user ->
                sessionFactory.withSession(session -> {
                    user.setFullName(obj.getFullName());
                    user.setPost(obj.getPost());
                    return session.persist(user).onItem().transform(v -> user);
                }
            )
        ).convert().with(UniReactorConverters.toMono());
    }

    @Override
    public Mono<Boolean> deleteById(UUID id) {
        var criteriaBuilder = sessionFactory.getCriteriaBuilder();
        CriteriaDelete<User> deleteQuery =  criteriaBuilder.createCriteriaDelete(User.class);
        Root<User> root = deleteQuery.from(User.class);
        deleteQuery.where(root.get("id").in(Collections.singletonList(id)));
        return sessionFactory.withSession(session ->
            session.createQuery(deleteQuery).executeUpdate().onItem().transform(i -> i == 1)
        ).convert().with(UniReactorConverters.toMono());
    }

    @Override
    public Mono<Void> delete(User obj) {
        return sessionFactory.withSession(session ->
                session.remove(obj)
        ).convert().with(UniReactorConverters.toMono());
    }

    private Uni<User> uniGetById(UUID id) {
        return sessionFactory.withSession(
                session -> session.find(User.class, id)
        );
    }

    @Override
    public Mono<List<User>> getAll() {
        return sessionFactory.withSession(session ->
            session.createQuery("SELECT u FROM User u", User.class).getResultList()
        ).convert().with(UniReactorConverters.toMono());
    }
}
