package com.sample.cml.client;

import com.sample.cml.service.dto.out.AccountDraftDto;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface AccountServiceClient {
    Mono<Boolean> createAccount(AccountDraftDto draftDto);
}
