package com.sample.cml.client.impl;

import com.sample.cml.client.AccountServiceClient;
import com.sample.cml.service.config.UserServiceClientConfig;
import com.sample.cml.service.dto.in.AccountDto;
import com.sample.cml.service.dto.out.AccountDraftDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.util.function.Predicate;


@Component
public class AccountServiceClientImpl implements AccountServiceClient {

    private final WebClient accountServiceClient;

    @Autowired
    public AccountServiceClientImpl(WebClient.Builder webClientBuilder, UserServiceClientConfig clientConfig) {
        accountServiceClient = webClientBuilder.baseUrl(clientConfig.getUrl()).build();
    }

    @Override
    public Mono<Boolean> createAccount(AccountDraftDto draftDto) {
        return accountServiceClient.post().uri("/v1/account")
                .bodyValue(draftDto).retrieve()
                .onStatus(Predicate.not(HttpStatus.OK::equals), httpStatusCode -> {
                        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Fail to create account");
                    }
                ).bodyToMono(AccountDto.class).map(v -> true);
    }
}
