package com.sample.cml.client.impl;

import com.sample.cml.client.UserServiceClient;
import com.sample.cml.service.config.UserServiceClientConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.util.UUID;
import java.util.function.Predicate;


@Component
public class UserServiceClientImpl implements UserServiceClient {

    private final WebClient userServiceClient;

    @Autowired
    public UserServiceClientImpl(WebClient.Builder webClientBuilder, UserServiceClientConfig clientConfig) {
        userServiceClient = webClientBuilder.baseUrl(clientConfig.getUrl()).build();
    }

    @Override
    public Mono<Boolean> verifyUser(UUID userId) {
        return userServiceClient.get().uri(uriBuilder ->
                        uriBuilder
                                .path("/user/{id}")
                                .build(userId
                                )
                ).accept(MediaType.APPLICATION_JSON).retrieve()
                .onStatus(Predicate.not(HttpStatus.OK::equals), resp -> {
                            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User does not verified");
                        }
                ).bodyToMono(Boolean.class);
    }
}
