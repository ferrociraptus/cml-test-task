package com.sample.cml.client;

import reactor.core.publisher.Mono;

import java.util.UUID;

public interface UserServiceClient {
    Mono<Boolean> verifyUser(UUID userId);
}
