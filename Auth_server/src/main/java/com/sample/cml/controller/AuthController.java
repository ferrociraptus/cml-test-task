package com.sample.cml.controller;


import com.sample.cml.service.dto.in.CredentialsDto;
import com.sample.cml.service.dto.out.TokenDto;
import com.sample.cml.service.security.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1")
public class AuthController {

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/login")
    public Mono<TokenDto> loginUser(@RequestBody CredentialsDto credentials) {
        return authService.authenticateUser(credentials.login(), credentials.password());
    }

    @PostMapping("/logout")
    public Mono<Void> logoutUser(@RequestParam String refreshToken, SecurityContext securityContext) {
        return authService.logout(refreshToken, securityContext.getAuthentication().getCredentials().toString());
    }
}
