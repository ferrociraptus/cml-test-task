package com.sample.cml.controller;

import com.sample.cml.service.dto.in.AccountDraftDto;
import com.sample.cml.service.dto.out.AccountDto;
import com.sample.cml.service.entity.Account;
import com.sample.cml.service.subject.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;


@RestController
@RequestMapping("/v1")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/account")
    public Mono<AccountDto> createAccount(@RequestBody AccountDraftDto accountDraft) {
        return accountService.createNewAccount(accountDraft).map(a -> new AccountDto(a.getId(), a.getLogin()));
    }
}
