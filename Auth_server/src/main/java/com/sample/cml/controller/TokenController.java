package com.sample.cml.controller;


import com.sample.cml.service.dto.out.TokenDto;
import com.sample.cml.service.security.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1")
public class TokenController {

    @Autowired
    private AuthService authService;

    @GetMapping("/token")
    public Mono<TokenDto> renewSessionToken(@RequestParam String refreshToken) {
        return authService.getSessionJwt(refreshToken);
    }
}
