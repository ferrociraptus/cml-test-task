package com.sample.cml.service.security;

import com.sample.cml.service.entity.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.util.Collections;

@Lazy
@Component
public class AuthenticationManager implements ReactiveAuthenticationManager {

    private final AuthService authService;

    @Autowired
    public AuthenticationManager(AuthService jwtService) {
        this.authService = jwtService;
    }

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        String jwtToken = authentication.getCredentials().toString();
        return authService.validateToken(jwtToken).map( status -> {
                    if (status.getValid()) {
                        return authentication;
                    } else {
                        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Session JWT is expired");
                    }
                }
        );
    }

    private UsernamePasswordAuthenticationToken getAuthorities(Account userAuthorities) {
        return new UsernamePasswordAuthenticationToken(
                userAuthorities.getId(), userAuthorities.getLogin(),
                Collections.singletonList(new SimpleGrantedAuthority("USER"))
        );
    }
}