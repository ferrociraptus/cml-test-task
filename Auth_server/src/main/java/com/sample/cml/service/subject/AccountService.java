package com.sample.cml.service.subject;

import com.sample.cml.service.dto.in.AccountDraftDto;
import com.sample.cml.service.entity.Account;
import reactor.core.publisher.Mono;

public interface AccountService {

    Mono<Account> createNewAccount(AccountDraftDto accountDraft);
}
