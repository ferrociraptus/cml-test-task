package com.sample.cml.service.dto.in;

public record CredentialsDto(
    String login,
    String password
) { }
