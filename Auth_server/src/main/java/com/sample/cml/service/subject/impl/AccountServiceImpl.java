package com.sample.cml.service.subject.impl;

import com.sample.cml.client.UserServiceClient;
import com.sample.cml.repository.AccountRepository;
import com.sample.cml.service.dto.in.AccountDraftDto;
import com.sample.cml.service.entity.Account;
import com.sample.cml.service.subject.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.util.Logger;
import reactor.util.Loggers;

import java.util.Objects;


@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository repository;
    private final UserServiceClient userServiceClient;
    private final Logger logger = Loggers.getLogger(AccountService.class);
    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Autowired
    public AccountServiceImpl(
            AccountRepository repository, UserServiceClient userServiceClient) {
        this.repository = repository;
        this.userServiceClient = userServiceClient;
    }

    @Override
    public Mono<Account> createNewAccount(AccountDraftDto accountDraft) {
        return userServiceClient.verifyUser(accountDraft.getUserId())
                .flatMap(isExist -> repository.create(
                        new Account(
                                accountDraft.getUserId(),
                                accountDraft.getLogin(),
                                passwordEncoder.encode(accountDraft.getPassword()))
                        )
                ).doOnEach( account -> logger.info("New Account[%s] created",
                        Objects.requireNonNull(account.get()).getId().toString())
                );
    }
}
