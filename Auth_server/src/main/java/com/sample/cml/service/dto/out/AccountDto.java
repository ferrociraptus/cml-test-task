package com.sample.cml.service.dto.out;

import java.util.UUID;

public class AccountDto {
    private UUID userId;
    private String login;

    public AccountDto(UUID userId, String login) {
        this.userId = userId;
        this.login = login;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
