package com.sample.cml.service.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class JwtConfig {

    @Value("#{T(java.time.Duration).parse('${cml.jwt.refresh-token.expiration-time}')}")
    private Duration refreshJwtExpirationTime;

    @Value("#{T(java.time.Duration).parse('${cml.jwt.session-token.expiration-time}')}")
    private Duration sessionJwtExpirationTime;

    @Value("${cml.jwt.rsa-private-key-file}")
    private String privateKeyFilePath;

    @Value("${cml.jwt.rsa-public-key-file}")
    private String publicKeyFilePath;

    @Value("${cml.jwt.issuer:cml-sample-task}")
    private String issuer;

    public JwtConfig() {
    }

    public Duration getRefreshJwtExpirationTime() {
        return refreshJwtExpirationTime;
    }

    public Duration getSessionJwtExpirationTime() {
        return sessionJwtExpirationTime;
    }

    public String getPublicKeyFilePath() {
        return publicKeyFilePath;
    }

    public String getPrivateKeyFilePath() {
        return privateKeyFilePath;
    }

    public String getIssuer() {
        return issuer;
    }
}
