package com.sample.cml.service.dto.out;

import com.sample.cml.service.dto.TokenType;

public record TokenDto(
    String tokenId,
    TokenType tokenType,
    String token,
    String refreshToken
) { }
