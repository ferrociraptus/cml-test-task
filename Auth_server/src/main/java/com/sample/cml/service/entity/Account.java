package com.sample.cml.service.entity;

import jakarta.persistence.*;

import java.util.UUID;

@Entity(name = "Account")
@Table(name = "account", schema = "cml_auth")
public class Account {

    @Id
    @Column(name = "user_id")
    private UUID id;

    @Column(columnDefinition = "TEXT")
    private String login;

    @Column(columnDefinition = "TEXT")
    private String password;

    // default constructor for hibernate reactive entity
    protected Account() { }

    public Account(UUID id, String login, String password) {
        this.id = id;
        this.login = login;
        this.password = password;
    }

    public UUID getId() {
        return id;
    }

    protected void setId(UUID id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
