package com.sample.cml.service.dto.out;

public class TokenStatus {

    private Boolean isValid;
    private String issuer;

    protected TokenStatus() {}

    public TokenStatus(Boolean isValid, String issuer) {
        this.isValid = isValid;
        this.issuer = issuer;
    }

    public Boolean getValid() {
        return isValid;
    }

    public String getIssuer() {
        return issuer;
    }
}
