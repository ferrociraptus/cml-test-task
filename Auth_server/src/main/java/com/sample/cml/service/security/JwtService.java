package com.sample.cml.service.security;

import com.sample.cml.service.config.JwtConfig;
import com.sample.cml.service.entity.Account;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.Duration;
import java.util.*;
import java.util.function.Function;

@Service
public class JwtService {

    private final PublicKey publicRsaKey;

    private final PrivateKey privateRsaKey;

    private final JwtConfig config;

    @Autowired
    public JwtService(JwtConfig config) throws Exception {
        this.config = config;
        publicRsaKey = getPublicKey(config.getPublicKeyFilePath());
        privateRsaKey = getPrivateKey(config.getPrivateKeyFilePath());
    }

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public String extractIssuer(String token) {
        return extractClaim(token, Claims::getIssuer);
    }

    public String extractID(String token) {
        return extractClaim(token, Claims::getId);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    public <T> T extractClaim(String token, String claim, Class<T> objType) {
        final Claims claims = extractAllClaims(token);
        return claims.get(claim, objType);
    }

    public String generateSessionToken(UserDetails userDetails) {
        return generateSessionToken(new HashMap<>(), userDetails);
    }

    public String generateSessionToken(Map<String, Object> extraClaims, UserDetails userDetails) {
        return buildToken(extraClaims, userDetails, config.getSessionJwtExpirationTime());
    }

    public String generateSessionToken(Account userDetails) {
        return generateSessionToken(new HashMap<>(), userDetails);
    }

    public String generateSessionToken(Map<String, Object> extraClaims, Account userDetails) {
        return buildToken(extraClaims, userDetails, config.getSessionJwtExpirationTime());
    }

    public String generateRefreshToken(Account userDetails) {
        return buildToken(
                Collections.singletonMap("id", userDetails.getId()),
                userDetails,
                config.getRefreshJwtExpirationTime()
        );
    }

    private String buildToken(
            Map<String, Object> extraClaims,
            UserDetails userDetails,
            Duration tokenLiveTime
    ) {
        return buildToken(
                extraClaims,
                new Account(UUID.randomUUID(), userDetails.getUsername(), userDetails.getPassword()),
                tokenLiveTime
        );
    }

    private String buildToken(
            Map<String, Object> extraClaims,
            Account userDetails,
            Duration tokenLiveTime
    ) {
        return Jwts
                .builder()
                .id(UUID.randomUUID().toString())
                .issuer(config.getIssuer())
                .issuedAt(new Date(System.currentTimeMillis()))
                .claims(extraClaims)
                .claim("userId", userDetails.getId())
                .subject(userDetails.getLogin())
                .expiration(new Date(System.currentTimeMillis() + tokenLiveTime.toMillis()))
                .signWith(privateRsaKey, Jwts.SIG.RS256)
                .compact();
    }

    public boolean isTokenValid(String token) {
        return !isTokenExpired(token);
    }

    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    private Claims extractAllClaims(String token) {
        return Jwts
                .parser()
                .verifyWith(publicRsaKey)
                .build()
                .parseSignedClaims(token)
                .getPayload();
    }

    public static RSAPublicKey getPublicKey(String filepath) throws Exception {
        String key = Files.readString(Paths.get(filepath), Charset.defaultCharset());

        String publicKeyPEM = key
                .replace("-----BEGIN PUBLIC KEY-----", "")
                .replaceAll(System.lineSeparator(), "")
                .replace("-----END PUBLIC KEY-----", "");

        byte[] encoded = Base64.getDecoder().decode(publicKeyPEM);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
        return (RSAPublicKey) keyFactory.generatePublic(keySpec);
    }

    public static RSAPrivateKey getPrivateKey(String filepath) throws Exception {
        String key = Files.readString(Paths.get(filepath), Charset.defaultCharset());

        String privateKeyPEM = key
                .replace("-----BEGIN PRIVATE KEY-----", "")
                .replaceAll(System.lineSeparator(), "")
                .replace("-----END PRIVATE KEY-----", "");

        byte[] encoded = Base64.getDecoder().decode(privateKeyPEM);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
        return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
    }
}