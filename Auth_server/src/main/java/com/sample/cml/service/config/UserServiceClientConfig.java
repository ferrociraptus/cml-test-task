package com.sample.cml.service.config;

import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties("cml.user-service")
public class UserServiceClientConfig{

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
