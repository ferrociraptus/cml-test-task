package com.sample.cml.service.security.impl;

import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.sample.cml.repository.AccountRepository;
import com.sample.cml.service.config.JwtConfig;
import com.sample.cml.service.dto.TokenType;
import com.sample.cml.service.dto.out.TokenDto;
import com.sample.cml.service.dto.out.TokenStatus;
import com.sample.cml.service.entity.Account;
import com.sample.cml.service.security.AuthService;
import com.sample.cml.service.security.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;
import reactor.util.Logger;
import reactor.util.Loggers;

import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class AuthServiceImpl implements AuthService, ReactiveUserDetailsService {
    private final AccountRepository accountRepository;
    private final JwtService jwtService;
    private static final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    private final AsyncLoadingCache<String, TokenStatus> jwtStatusCache;     // used as black list too
    private final Logger logger = Loggers.getLogger(AuthService.class);


    @Autowired
    public AuthServiceImpl(
            AccountRepository accountRepository,
            JwtService jwtService,
            JwtConfig jwtConfig
    ) {
        this.accountRepository = accountRepository;
        this.jwtService = jwtService;
        this.jwtStatusCache = Caffeine.newBuilder()
                .expireAfterWrite(jwtConfig.getRefreshJwtExpirationTime())
                .buildAsync((token, executor) -> jwtService.isTokenValid(token)
                        ? CompletableFuture.completedFuture(
                                new TokenStatus(true, jwtService.extractIssuer(token)))
                        : CompletableFuture.completedFuture(new TokenStatus(false, "")));
    }


    @Override
    public Mono<TokenDto> authenticateUser(String login, String password) {
        return accountRepository.getByLogin(login).map( account -> {
                if (!BCrypt.checkpw(password, account.getPassword())) {
                    throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "The credentials are prohibited");
                }
                var sessionToken = jwtService.generateSessionToken(account);
                return new TokenDto(
                        jwtService.extractID(sessionToken),
                        TokenType.IdToken,
                        sessionToken,
                        jwtService.generateRefreshToken(account)
                );
            }
        ).doOnEach(token -> logger.info("User with login \"%s\" are login in system"));
    }

    @Override
    public Mono<TokenDto> getSessionJwt(String refreshToken) {
        if (jwtService.isTokenValid(refreshToken)) {
            Account account = new Account(
                    UUID.fromString(jwtService.extractClaim(refreshToken, "id", String.class)),
                    jwtService.extractUsername(refreshToken),
                    "");
            var sessionToken = jwtService.generateSessionToken(account);
            return Mono.just(
                    new TokenDto(
                            jwtService.extractID(sessionToken),
                            TokenType.IdToken,
                            sessionToken,
                            refreshToken
                    )
            );
        }
        else {
            return Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid session token is passed"));
        }
    }

    @Override
    public Mono<Void> logout(String refreshToken) {
        return logout(refreshToken, null);
    }

    @Override
    public Mono<Void> logout(String refreshToken, String sessionToken) {
        if (!jwtService.isTokenValid(refreshToken)) {
            return Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Passed refresh token is prohibited"));
        } else if (sessionToken != null && jwtService.isTokenValid(sessionToken)) {
            return Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Passed session token is prohibited"));
        }

        jwtStatusCache.put(refreshToken, CompletableFuture.completedFuture(new TokenStatus(false, "")));
        if (sessionToken != null) {
            jwtStatusCache.put(sessionToken, CompletableFuture.completedFuture(new TokenStatus(false, "")));
        }
        return Mono.empty();
    }

    @Override
    public Mono<TokenStatus> validateToken(String anyToken) {
        return Mono.fromCompletionStage(jwtStatusCache.get(anyToken));
    }

    @Override
    public Mono<UserDetails> findByUsername(String username) {
            return accountRepository.getByLogin(username).map(account ->
                    new User(
                            account.getLogin(),
                            account.getPassword(),
                            Collections.singletonList(
                                    new SimpleGrantedAuthority("ROLE_USER")
                            )
                    )
            );
    }
}
