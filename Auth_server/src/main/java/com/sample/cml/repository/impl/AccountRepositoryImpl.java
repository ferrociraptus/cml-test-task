package com.sample.cml.repository.impl;

import com.sample.cml.repository.AccountRepository;
import com.sample.cml.service.entity.Account;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.converters.uni.UniReactorConverters;
import jakarta.persistence.criteria.CriteriaDelete;
import jakarta.persistence.criteria.Root;
import org.hibernate.reactive.mutiny.Mutiny;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.UUID;


@Repository
public class AccountRepositoryImpl extends AccountRepository {

    private final Mutiny.SessionFactory sessionFactory;

    @Autowired
    public AccountRepositoryImpl(Mutiny.SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Mono<Account> getById(UUID uuid) {
        return uniGetById(uuid)
                .convert().with(UniReactorConverters.toMono());
    }

    @Override
    public Mono<Account> create(Account obj) {
        return sessionFactory.withSession(session ->
                session.persist(obj)
                        .onItem().call(v -> session.flush())
            ).onItem().transform(v -> obj)
            .convert().with(UniReactorConverters.toMono());
    }

    @Override
    public Mono<Account> update(Account obj) {
        return uniGetById(obj.getId()).onItem().transformToUni( account ->
                sessionFactory.withSession(session -> {
                    account.setLogin(obj.getLogin());
                    account.setPassword(obj.getPassword());
                    return session.persist(account)
                            .onItem().transform(v -> account);
                }
            )
        ).convert().with(UniReactorConverters.toMono());
    }

    @Override
    public Mono<Boolean> deleteById(UUID id) {
        var criteriaBuilder = sessionFactory.getCriteriaBuilder();
        CriteriaDelete<Account> deleteQuery =  criteriaBuilder.createCriteriaDelete(Account.class);
        Root<Account> root = deleteQuery.from(Account.class);
        deleteQuery.where(root.get("id").in(Collections.singletonList(id)));
        return sessionFactory.withSession(session ->
            session.createQuery(deleteQuery).executeUpdate().onItem().transform(i -> i == 1)
        ).convert().with(UniReactorConverters.toMono());
    }

    @Override
    public Mono<Void> delete(Account obj) {
        return sessionFactory.withSession(session ->
                session.remove(obj)
        ).convert().with(UniReactorConverters.toMono());
    }

    @Override
    public Mono<Account> getByLogin(String login) {
        var cb = sessionFactory.getCriteriaBuilder();
        var query = cb.createQuery(Account.class);
        var root = query.from(Account.class);

        return sessionFactory.withSession(session ->
                session.createQuery(query.select(root).where(cb.equal(root.get("login"), login)))
                        .getSingleResult()
        ).convert().with(UniReactorConverters.toMono());
    }

    private Uni<Account> uniGetById(UUID id) {
        return sessionFactory.withSession(
                session -> session.find(Account.class, id)
        );
    }
}
