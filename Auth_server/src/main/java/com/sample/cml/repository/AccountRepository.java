package com.sample.cml.repository;

import com.sample.cml.service.entity.Account;
import reactor.core.publisher.Mono;

import java.util.UUID;

abstract public class AccountRepository implements AsyncCrudInterface<UUID, Account> {

    public abstract Mono<Account> getByLogin(String login);
}
