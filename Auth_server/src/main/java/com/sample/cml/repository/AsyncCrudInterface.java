package com.sample.cml.repository;

import reactor.core.publisher.Mono;

public interface AsyncCrudInterface<ID, T> {
    Mono<T> getById(ID id);

    Mono<T> create(T obj);

    Mono<T> update(T obj);

    Mono<Boolean> deleteById(ID id);

    Mono<Void> delete(T obj);
}
