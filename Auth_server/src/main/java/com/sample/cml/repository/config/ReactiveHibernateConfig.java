package com.sample.cml.repository.config;


import jakarta.persistence.Persistence;
import org.hibernate.reactive.mutiny.Mutiny;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class ReactiveHibernateConfig {

    @Bean
    public Mutiny.SessionFactory sessionFactory(
            @Value("${spring.datasource.url: }") String url,
            @Value("${spring.datasource.username: }") String user,
            @Value("${spring.datasource.password: }") String password
    ) {
        var hibernateConfig = new HashMap<String, String>();
        if (!url.isBlank())
            hibernateConfig.put("javax.persistence.jdbc.url", url);
        if (!user.isBlank())
            hibernateConfig.put("javax.persistence.jdbc.user", user);
        if (!password.isBlank())
            hibernateConfig.put("javax.persistence.jdbc.password", password);

        return Persistence.createEntityManagerFactory("entityManager", hibernateConfig)
                .unwrap(Mutiny.SessionFactory.class);
    }
}
